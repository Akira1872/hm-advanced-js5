// fetch
function fetchData(url){
    return fetch(url)
    .then(response => {
        if(!response.ok) throw new Error ('Error not found');
        return response.json()
    })
    .catch (err =>{
        console.error('Error:', err);
        throw err;
    });
}

class Card {
    constructor (id, name, username, email, userId, title, body){
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }
}

// create List
function createList (id, card) {
    const ul = document.querySelector('.list');
    const cardCreate = document.createElement('li');
    cardCreate.classList.add('user');
    const divTitle = document.createElement('div');
    divTitle.classList.add('title')
    
    const cardUserName = document.createElement('h2');
    cardUserName.textContent = card.username;
    cardUserName.classList.add('user_nik');
    divTitle.append(cardUserName);

    const cardName = document.createElement('h3');
    cardName.textContent = card.name;
    cardName.classList.add('user_name');
    divTitle.append(cardName);

    const cardEmail = document.createElement('h3');
    cardEmail.textContent = card.email;
    cardEmail.classList.add('user_email');
    cardCreate.append(cardEmail);

    const cardTitle = document.createElement('span');
    cardTitle.textContent = card.title;
    cardTitle.classList.add('post_title');
    cardCreate.append(cardTitle);

    const cardContent = document.createElement('p');
    cardContent.textContent = card.body;
    cardContent.classList.add('post');
    cardCreate.append(cardContent);

    cardCreate.prepend(divTitle);
    ul.append(cardCreate);
    // divTitle.insertAdjacentHTML('beforeend', `
    // <svg class="edit" width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    //     <g id="SVGRepo_bgCarrier" stroke-width="0"/>
    //     <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"/>
    //     <g id="SVGRepo_iconCarrier"> <path d="M21.2799 6.40005L11.7399 15.94C10.7899 16.89 7.96987 17.33 7.33987 16.7C6.70987 16.07 7.13987 13.25 8.08987 12.3L17.6399 2.75002C17.8754 2.49308 18.1605 2.28654 18.4781 2.14284C18.7956 1.99914 19.139 1.92124 19.4875 1.9139C19.8359 1.90657 20.1823 1.96991 20.5056 2.10012C20.8289 2.23033 21.1225 2.42473 21.3686 2.67153C21.6147 2.91833 21.8083 3.21243 21.9376 3.53609C22.0669 3.85976 22.1294 4.20626 22.1211 4.55471C22.1128 4.90316 22.0339 5.24635 21.8894 5.5635C21.7448 5.88065 21.5375 6.16524 21.2799 6.40005V6.40005Z" stroke="#faf4fa" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/> <path d="M11 4H6C4.93913 4 3.92178 4.42142 3.17163 5.17157C2.42149 5.92172 2 6.93913 2 8V18C2 19.0609 2.42149 20.0783 3.17163 20.8284C3.92178 21.5786 4.93913 22 6 22H17C19.21 22 20 20.2 20 18V13" stroke="#faf4fa" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/> </g>
    // </svg>`); 

    divTitle.insertAdjacentHTML('beforeend', `
    <svg class="delete" width="20px" height="20px" viewBox="0 0 1024 1024" class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#000000">
        <g id="SVGRepo_bgCarrier" stroke-width="0"/>
        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"/>
        <g id="SVGRepo_iconCarrier">
            <path d="M905.92 237.76a32 32 0 0 0-52.48 36.48A416 416 0 1 1 96 512a418.56 418.56 0 0 1 297.28-398.72 32 32 0 1 0-18.24-61.44A480 480 0 1 0 992 512a477.12 477.12 0 0 0-86.08-274.24z" fill="#f7f7f7"/>
            <path d="M630.72 113.28A413.76 413.76 0 0 1 768 185.28a32 32 0 0 0 39.68-50.24 476.8 476.8 0 0 0-160-83.2 32 32 0 0 0-18.24 61.44zM489.28 86.72a36.8 36.8 0 0 0 10.56 6.72 30.08 30.08 0 0 0 24.32 0 37.12 37.12 0 0 0 10.56-6.72A32 32 0 0 0 544 64a33.6 33.6 0 0 0-9.28-22.72A32 32 0 0 0 505.6 32a20.8 20.8 0 0 0-5.76 1.92 23.68 23.68 0 0 0-5.76 2.88l-4.8 3.84a32 32 0 0 0-6.72 10.56A32 32 0 0 0 480 64a32 32 0 0 0 2.56 12.16 37.12 37.12 0 0 0 6.72 10.56zM726.72 297.28a32 32 0 0 0-45.12 0l-169.6 169.6-169.28-169.6A32 32 0 0 0 297.6 342.4l169.28 169.6-169.6 169.28a32 32 0 1 0 45.12 45.12l169.6-169.28 169.28 169.28a32 32 0 0 0 45.12-45.12L557.12 512l169.28-169.28a32 32 0 0 0 0.32-45.44z" fill="#f7f7f7"/></g>
    </svg>`);

    const deleteIcon = divTitle.querySelector('.delete'); 
    deleteIcon.addEventListener('click', async () => {
    const postId = card.id; 
    const deleteResponse = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE',
    });
    if (deleteResponse.ok) cardCreate.remove();
});
    document.querySelector('.loader').style.display = 'none';
}

// file overwriting
async function createCards(){
        const usersData = await fetchData('https://ajax.test-danit.com/api/json/users');
        const contentData = await fetchData('https://ajax.test-danit.com/api/json/posts');
        let cardId = 1;
        usersData.forEach(user => {
            const content = contentData.filter(post => post.userId === user.id);
            content.forEach(e => {
                const card = new Card(user.id, user.name, user.username, user.email, e.userId, e.title, e.body);                
                createList(cardId -1, card);
            });
        });
};
createCards();  
